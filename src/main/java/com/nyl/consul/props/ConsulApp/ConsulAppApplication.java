package com.nyl.consul.props.ConsulApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RestController;

import com.nyl.consul.props.objects.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.MediaType;
import org.springframework.vault.annotation.VaultPropertySource;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponseSupport;
import org.springframework.cloud.bootstrap.config.PropertySourceBootstrapProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.netflix.hystrix.HystrixCommand;
//import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;
//import org.springframework.cloud.sleuth.zipkin.*;

//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
// import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import com.amazonaws.services.kms.AWSKMS;
//import com.amazonaws.services.kms.AWSKMSClient;
//import com.amazonaws.regions.Region;
// import com.amazonaws.regions.Regions;

//import org.springframework.cloud.sleuth.zipkin.

// @ConditionalOnProperty(prefix = "aws.kms", name = "enabled", havingValue = "true", matchIfMissing = true)
@RestController
@EnableDiscoveryClient
@SpringBootApplication
@RefreshScope
@EnableAutoConfiguration
@VaultPropertySource("secret/nyl-application")
@EnableConfigurationProperties(PropertySourceBootstrapProperties.class)
@EnableCircuitBreaker

//@EnableZipkinStreamServer
public class ConsulAppApplication {
	
	 // VaultTemplate vaultTemplate = new VaultTemplate(new VaultEndpoint(),
         //     new TokenAuthentication("991399bc-f112-ee1e-4851-186a83b9845b"));
	
	 private final Logger logger = LoggerFactory.getLogger(this.getClass());

      Secrets secrets = new Secrets();
//	 @Value(value = "${server.url}")
	  
	 
 
	//String pwd;
	//@Autowired
	//private Environment env;
   //@Value(value = "${kafkamsgsvr}")
   @Value(value = "${kafkamsgsvrurl}")
	String url;
   

	
	// @Value(value = "${sample}")
	// String sample;
	
	@Value(value = "${kafkaPassword}")
	String pwd;
	
	
	//@Value(value = "${aws.kms.kafkaurl:}")
	String kafkaurl="LOaded";
	
  //String message;
//	String pwd=env.getProperty("password");
	 //System.out.println("ENVIRONMENT" + message);
	//VaultResponseSupport<Secrets> response = vaultTemplate.read("secret/password", Secrets.class);
	
	  @RequestMapping(value = "/PropertiesManagement", produces = MediaType.TEXT_HTML_VALUE)
	//  @HystrixCommand(fallbackMethod = "doFallback", commandKey = "configurationNYLLoader")
	  public String configurationNYLLoader() {
		  
		  logger.debug("NYLDEBUG This is a debug message");
	        logger.info("NYLINFO This is an info message");
	        logger.warn("NYLWARN This is a warn message");
	        logger.error("NYLERROR This is an error message");
  //       
	        return "<center><b><font color=green size=6>Externalize Properties in to Consul &  Vault</font></b><b><br><font color=red size=10>URL<From Git & Consul> "+url+"</font></b><b><br><font color=blue size=10>Secured Password<Vault>" + pwd +"</font></b> <b><font color=green size=10>retrieved</font></b></center>";
	    }
	
	private String doFallback()
		
	{
	    return "<center><b><font color=green size=6>Service is unavailable termporarily</font></b></center>";
		
	}
	


	public static void main(String[] args) {
		SpringApplication.run(ConsulAppApplication.class, args);
	}
}

