package com.nyl.consul.controller;

import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
public class ConfigurationController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
    
    @RequestMapping("/logs")
    public String index() {
    	
    	// logger.debug("This is a debug message GETTING URL"+resource.getUrl());
        // logger.info("This is a INFO message GETTING URL\"+resource.getUrl());
         logger.warn("ControllerWarn");
         logger.error("Error");
         
    	          
        return "Greetings from  Configuration Frame work!";
    }
    
}
